function findMissingNumber(arr1) {
    let missing = 0

    for (let i=0;i<=arr1.length;i++){
        if(!arr1.includes(i)){
            missing=i
        }
    }

    return missing
}

module.exports = findMissingNumber;
