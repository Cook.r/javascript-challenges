function findMissingLetter(arr1) {
    let start = arr1[0].charCodeAt(0)

    for (let i=0;i<arr1.length;i++){
        const current = arr1[i].charCodeAt(0)

        if (current - start >1) {
            return String.fromCharCode(start+1)
        }
        start = current
    }

    return ""
}

module.exports = findMissingLetter;
